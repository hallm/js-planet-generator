import Vuex from 'vuex';

function createStore() {
  return new Vuex.Store({
    state: {
      stars: [],
      polygons: [],
      starById: {},
      seed: 0
    },

    mutations: {
      setStarData(state, starData) {
        state.stars = starData.stars;
        state.polygons = starData.polygons;
        state.starById = starData.starById;
        state.seed = starData.seed;
      }
    }
  });
};

export default createStore;

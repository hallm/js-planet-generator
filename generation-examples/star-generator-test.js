const generateStarInfo = require('../generator/star-generation/');

// const seed = 54232;
const seed = (Math.random() * 100000) | 0;

console.log('seed:', seed);
const starInfo = generateStarInfo(seed);

console.log(JSON.stringify(starInfo, null, 2));

'use strict';

const fs = require('fs');
const path = require('path');
const generatePlanetInfo = require('../generator/planet-generation/');
const terrainGeneration = require('../generator/terrain-generation/');
const randomInt = require('../generator/utils/random-int');
const Mercator = require('../generator/utils/mercator');

const showMirrorPartsOfMap = false;

const seed = 61290;//54060;
// const seed = (Math.random() * 10000) | 0;

// 3017 uses up all the rolls?!

console.log('seed:', seed);

const starInfo = {
  starType: 'G3',
  starMass: 1.00,
  starLum: 1.15,
  starRad: 0.99,
  starTemp: 5670.00,
  starHabMod: 0,
  minLifeDist:  0.62,
  maxLifeDist: 1.25
};

const planetInfo = generatePlanetInfo(seed, starInfo, {
  orbitalSlot: 3,
  orbitalDistance: 1.00
}, {
  // planetTemperature: 297,
  // percentSurfaceWater: 50
});

// let planetInfo = null;
// do {
//   const innerseed = (Math.random() * 100000) | 0;

//   console.log('seed:', innerseed);
//   planetInfo = generatePlanetInfo(innerseed, starInfo, {
//     orbitalSlot: 3,
//     orbitalDistance: 1.00
//   });

//   if (planetInfo.planetType !== 'Terrestrial' || planetInfo.habitable === false) {
//     planetInfo = null;
//   }
// } while (planetInfo === null);
console.log(planetInfo);
// throw new Error('sdf');

const {cells, idToCell, mapWidth, mapHeight} = terrainGeneration(seed, starInfo, planetInfo);

const mercator = new Mercator(planetInfo.planetDiameter / 2.0, mapWidth, mapHeight);

// need some random colors for the plates
const plateColorTable = [];
function getPlateColorForIndex(index) {
  for (let i = plateColorTable.length; i <= index; i++) {
    plateColorTable.push(randomColor());
  }
  return plateColorTable[index];
}

function hexifynum(num) {
  const v = '00' + (num.toString(16));
  return v.substring(v.length - 2);
}

const diagrams = [
  {
    name: 'coast',
    desc: 'Coastline',
    fn: coastSvgForCell
  },
  {
    name: 'plates',
    desc: 'Plates',
    fn: plateSvgForCell
  },
  {
    name: 'heightmap',
    desc: 'Heightmap',
    fn: heightmapSvgForCell
  },
  {
    name: 'wind',
    desc: 'Wind',
    fn: windSvgForCell,
    showGrid: true
  },
  {
    name: 'temperature',
    desc: 'Temperature',
    fn: temperatureSvgForCell
  },
  {
    name: 'moisture',
    desc: 'Moisture',
    fn: moistureSvgForCell
  },
  {
    name: 'biome',
    desc: 'Biomes',
    fn: biomeSvgForCell
  }
];

diagrams.forEach(function(diagramInfo) {
  buildSvg(diagramInfo.name, diagramInfo.fn, diagramInfo.showGrid);
});

generateHtml();

function randomColor() {
  const r = hexifynum(randomInt(Math.random, 0, 255));
  const g = hexifynum(randomInt(Math.random, 0, 255));
  const b = hexifynum(randomInt(Math.random, 0, 255));

  return '#' + r + g + b;
}

function buildSvg(name, svgForCell, showLatLngGrid) {
  const svgHeader = showMirrorPartsOfMap ?
    `<svg width="${mapWidth * 6 / 4}" height="${mapHeight}" xmlns="http://www.w3.org/2000/svg">\n`
    : `<svg width="${mapWidth}" height="${mapHeight}" xmlns="http://www.w3.org/2000/svg">\n`;
  const svgFooter = showMirrorPartsOfMap ?
    `<rect x="${mapWidth / 4}" y="0" width="${mapWidth}" height="${mapHeight}" fill="transparent" stroke="#000000" />\n</svg>\n`
    : '</svg>\n';

  let fileStr = svgHeader;
  cells.forEach(cell => {
    fileStr += svgForCell(cell);
  });

  if (showLatLngGrid) {
    // do the latitudes:
    [-60, -30, 0, 30, 60].forEach(function(lat) {
      const yVal = mercator.unproject({lat: lat, lng: 0}).y;
      fileStr += `<line x1="${0}" y1="${yVal}" x2="${mapWidth}" y2="${yVal}" stroke="#ffffff" />\n`;
    });
  }

  fileStr += svgFooter;

  const filePath = path.resolve(__dirname, `../svg-gen/${name}.svg`);
  fs.writeFileSync(filePath, fileStr, 'utf-8');
}

function coastSvgForCell(cell) {
  const actualCell = idToCell[cell.site._id];

  const fillcolor = actualCell.isWater ? '#0000ff' : '#009900';
  const strokecolor = fillcolor;

  const pathStr = cell.points.map(p => `${alterX(p.x)},${alterY(p.y)}`).join(' ');
  return `  <polygon data-cellid="${cell.site._id}" fill="${fillcolor}" stroke="${strokecolor}" points="${pathStr}"/>\n`;
}

function plateSvgForCell(cell) {
  const actualCell = idToCell[cell.site._id];

  const fillcolor = getPlateColorForIndex(actualCell.plateIndex) || '#ffffff';
  const strokecolor = fillcolor;
  const forcecolor = actualCell._startingPlate ? '#ff00ff' : '#000000';

  const cellForce = actualCell._force;

  const pathStr = cell.points.map(p => `${alterX(p.x)},${alterY(p.y)}`).join(' ');
  return `  <polygon data-cellid="${cell.site._id}" fill="${fillcolor}" stroke="${strokecolor}" points="${pathStr}"/>\n` +
         `  <rect x="${alterX(cell.site.x)}" y="${alterY(cell.site.y)}" width="1" height="1" stroke="#ff0000"/>\n` +
         `  <line x1="${alterX(cell.site.x)}" y1="${alterY(cell.site.y)}" x2="${alterX(cell.site.x + (cellForce.x))}" y2="${alterY(cell.site.y + (cellForce.y))}" stroke="${forcecolor}"/>\n`;
}

function heightmapSvgForCell(cell) {
  const actualCell = idToCell[cell.site._id];

  let fillcolor = actualCell.isWater ? '#0000dd' : '#00aa00';
  if (actualCell._mountainHeight != null) {
    fillcolor = actualCell.isWater ? '#0000ff' : '#cccccc';
  } else if (actualCell._valleyDepth != null) {
    fillcolor = actualCell.isWater ? '#0000bb' : '#008800';
  }

  const strokecolor = fillcolor;

  const pathStr = cell.points.map(p => `${alterX(p.x)},${alterY(p.y)}`).join(' ');
  return `  <polygon data-cellid="${cell.site._id}" fill="${fillcolor}" stroke="${strokecolor}" points="${pathStr}"/>\n`;
}

function windSvgForCell(cell) {
  const actualCell = idToCell[cell.site._id];

  const windVector = actualCell.windVector;

  const baseCellStr = heightmapSvgForCell(cell);

  return baseCellStr +
         `  <line x1="${alterX(cell.site.x)}" y1="${alterY(cell.site.y)}" x2="${alterX(cell.site.x + (windVector.x * 10))}" y2="${alterY(cell.site.y + (windVector.y * 10))}" stroke="#ffffff"/>\n`;
}

function temperatureSvgForCell(cell) {
  const temperatureColors = [
    '#e5e5e5',
    '#b2b2b2',
    '#bf7fff',
    '#7f00ff',
    '#0000ff',
    '#007fff',
    '#00ffff',
    '#00ff99',
    '#00ff00',
    '#b2ff00',
    '#ffff00',
    '#ffbf00',
    '#ffd1e0',
    '#f07091',
    '#ff0000'
  ];

  const actualCell = idToCell[cell.site._id];
  const cellTemperatureInF = actualCell.temperature * (9.0 / 5.0) - 459.67;
  let temperatureColorIndex = Math.floor(cellTemperatureInF / 10.0) + 4;
  if (temperatureColorIndex < 0) {
    temperatureColorIndex = 0;
  } else if (temperatureColorIndex >= temperatureColors.length) {
    temperatureColorIndex = temperatureColors.length - 1;
  }

  const fillcolor = temperatureColors[temperatureColorIndex];
  const strokecolor = fillcolor;

  const baseCellStr = heightmapSvgForCell(cell);

  const pathStr = cell.points.map(p => `${alterX(p.x)},${alterY(p.y)}`).join(' ');
  return baseCellStr +
         `  <polygon data-cellid="${cell.site._id}" fill="${fillcolor}" opacity="1.0" stroke="${strokecolor}" points="${pathStr}"/>\n`;
}

function moistureSvgForCell(cell) {
  const moistureColors = [
    '#000000',
    '#200000',
    '#480000',
    '#600000',
    '#780000',
    '#900000',
    '#a70000',
    '#c00000',
    '#d70000',
    '#ef0000'
  ];

  const actualCell = idToCell[cell.site._id];
  const cellMoisture = actualCell.moisture.contains;
  let moistureIndex = Math.floor(cellMoisture * 2.0);
  if (moistureIndex >= moistureColors.length) {
    moistureIndex = moistureColors.length - 1;
  } else if (moistureIndex < 0) {
    moistureIndex = 0;
  }

  const fillcolor = cell.isWater ? '#0000ff' : moistureColors[moistureIndex];
  const strokecolor = fillcolor;

  const baseCellStr = '';//heightmapSvgForCell(cell);
  const windVector = actualCell.windVector;

  const pathStr = cell.points.map(p => `${alterX(p.x)},${alterY(p.y)}`).join(' ');
  return baseCellStr +
         `  <polygon data-cellid="${cell.site._id}" fill="${fillcolor}" opacity="1" stroke="${strokecolor}" points="${pathStr}"/>\n` +
         `  <rect x="${alterX(cell.site.x - 1)}" y="${alterY(cell.site.y - 1)}" width="2" height="2" stroke="#00ff00"/>\n` +
         `  <line x1="${alterX(cell.site.x)}" y1="${alterY(cell.site.y)}" x2="${alterX(cell.site.x + (windVector.x * 10))}" y2="${alterY(cell.site.y + (windVector.y * 10))}" stroke="#ffffff"/>\n`;
}

function biomeSvgForCell(cell) {
  const biomeColors = {
    'Tropical Forest': '#9be023',
    'Tropical Rain Forest': '#09fa37',
    'Temperate Rain Forest': '#05f9a2',
    'Temperate Deciduous Forest': '#2fb153',
    'Desert': '#f79517',
    'Savana': '#fada05',
    'Tiaga': '#056621',
    'Tundra': '#54edf7',
    'Ice': '#ffffff',
    'Water': '#0000ff',
  };

  const actualCell = idToCell[cell.site._id];

  const fillcolor = biomeColors[actualCell.biome];
  const strokecolor = fillcolor;

  const baseCellStr = '';
  const windVector = actualCell.windVector;

  const pathStr = cell.points.map(p => `${alterX(p.x)},${alterY(p.y)}`).join(' ');
  return baseCellStr +
         `  <polygon data-cellid="${cell.site._id}" fill="${fillcolor}" opacity="1" stroke="${strokecolor}" points="${pathStr}"/>\n` +
         `  <rect x="${alterX(cell.site.x - 1)}" y="${alterY(cell.site.y - 1)}" width="2" height="2" stroke="#00ff00"/>\n` +
         `  <line x1="${alterX(cell.site.x)}" y1="${alterY(cell.site.y)}" x2="${alterX(cell.site.x + (windVector.x * 10))}" y2="${alterY(cell.site.y + (windVector.y * 10))}" stroke="#ffffff"/>\n`;
}

function alterX(value) {
  if (showMirrorPartsOfMap) {
    return value + (mapWidth / 4);
  } else {
    return value;
  }
}
function alterY(value) {
  return value;
}

function generateHtml() {
  const navElements = diagrams.map(d => `<li><a href="#" onclick="showSvg(event, 'map-${d.name}');">${d.desc}</a></li>`);
  const mapElements = diagrams.map((d, i) => `<img id="map-${d.name}" class="mapsvg ${i > 0 ? 'noshow' : ''}" src="./${d.name}.svg" />`);

  const templatePath = path.resolve(__dirname, '../svg-gen/_index.template.html');
  const templateString = fs.readFileSync(templatePath, 'utf-8');

  const fileStr = templateString
    .replace('{{NAV}}', navElements.join('\n'))
    .replace('{{MAPS}}', mapElements.join('\n'));

  const filePath = path.resolve(__dirname, '../svg-gen/index.html');
  fs.writeFileSync(filePath, fileStr, 'utf-8');
}

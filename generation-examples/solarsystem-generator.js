const generateSystemInfo = require('../generator/system-generation/');

const seed = 47272; // two habitable!
// const seed = (Math.random() * 100000) | 0;

// console.log('seed:', seed);
const systemInfo = generateSystemInfo(seed);
systemInfo.seed = seed;

console.log(JSON.stringify(systemInfo, null, 2));

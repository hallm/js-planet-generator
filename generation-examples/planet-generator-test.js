/*
this is for habitable planets, not moons or gas giants or dead worlds

player provides seed (S1) or random generated:
S1 is used for RNG1 to generate star positions using poisson
S1 + coordinates are fed into a hashing algo to generate SCn
for each star: SCn is used to generate the solar system features in an exact order
- SCn + orbital position, hashed, becomes SOn
- SOn is used to generate the planet's / asteroid belt features
- SOn + moon / asteroid (in belt) orbital position, hash, becomes SMn
- SMn is used to generate the moon's / asteroids features
*/

const generatePlanetInfo = require('../generator/planet-generation/');

// const seed = 54232;
const seed = (Math.random() * 100000) | 0;

console.log('seed:', seed);

const starInfo = {
  starType: 'G3',
  starMass: 1.00,
  starLum: 1.15,
  starRad: 0.99,
  starTemp: 5670.00,
  starHabMod: 0,
  minLifeDist:  0.62,
  maxLifeDist: 1.25
};

const planetInfo = generatePlanetInfo(seed, starInfo, {
  orbitalSlot: 3,
  orbitalDistance: 1.00
}, {});

console.log(JSON.stringify(planetInfo, null, 2));

const generateClusterInfo = require('../generator/cluster-generation/');

const seed = 54232;
// const seed = (Math.random() * 100000) | 0;

console.log('seed:', seed);
const systems = generateClusterInfo(seed, true);

const habitablePlanets = systems.reduce(function(s1, systemInfo) {
  return s1 + systemInfo.orbitals.reduce(function(s2, orbital) {
    if (orbital && orbital.habitable) {
      return s2 + 1;
    }
    return s2;
  }, 0);
}, 0);

const starTypes = [];
const countByTypes = {};

systems.forEach(function(star) {
  const starType = star.starInfo.starType + star.starInfo.starSubType + 'V';
  if (starTypes.indexOf(starType) === -1) {
    starTypes.push(starType);
  }

  if (!countByTypes[starType]) {
    countByTypes[starType] = 1;
  } else {
    countByTypes[starType]++;
  }
});

const typeInfo = starTypes.map(t => ({type: t, count: countByTypes[t]}));

console.log('total number of systems:', systems.length);
console.log('habitable planets:', habitablePlanets);
console.log(typeInfo);
// console.log(JSON.stringify(systems, null, 2));

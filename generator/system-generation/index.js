// generate star
// roll and generate orbitals (planets, asteroid-belts, empty)
// return {starInfo: starInfo, orbitals: []}
const xxhash = require('xxhashjs').h32;

const generateStarInfo = require('../star-generation/');
const generatePlanetInfo = require('../planet-generation/');

// ----------------------------------------------------------------------------
// Generation function
// ----------------------------------------------------------------------------
//
// inputs:
// rng: Function() -> number[0..1]
// overrides: {
//   starOverrides: {} (see star-generation for it's overrides into)
//   planetOverrides: [{...}] (see planet-generation. array index corresponds to orbit index)
//}
function generateSystemInfo(seed, overrides) {
  if (!overrides) {
    overrides = {
      starInfo: {},
      planetOverrides: []
    };
  }
  if (!overrides.planetOverrides) {
    overrides.planetOverrides = [];
  }
  if (!seed) {
    seed = (Math.random() * 1000000000) | 0;
  }

  const starInfo = generateStarInfo(seed, overrides.starOverrides);

  const orbitals = starInfo.orbitalSlots
    .map(function(orbitalDistance, orbitIndex) {
      const orbitCoordBuffer = Buffer.from([
        ((orbitIndex >> 8) & 0xFF),
        (orbitIndex & 0xFF),
        0,
        0
      ]);

      const orbitSeed = xxhash(orbitCoordBuffer, seed).toNumber();

      const orbitalInfo = generatePlanetInfo(orbitSeed, starInfo, {
        orbitalSlot: orbitIndex,
        orbitalDistance: orbitalDistance
      }, overrides.planetOverrides[orbitIndex]);

      orbitalInfo.seed = orbitSeed;

      return orbitalInfo;
    });

  return {
    starInfo: starInfo,
    orbitals: orbitals
  };
}

module.exports = generateSystemInfo;

'use strict';

const rngFactory = require('../utils/rng-factory');
const xxhash = require('xxhashjs').h32;
const overrideOrValue = require('../utils/override-or-value');
const rollnD6 = require('../utils/roll-n-d6');

// ----------------------------------------------------------------------------
// look up tables
// ----------------------------------------------------------------------------
const planetTypeTable = require('./tables/planet-types');

// ----------------------------------------------------------------------------
// Generation function
// ----------------------------------------------------------------------------
//
// inputs:
// rng: Function() -> number[0..1]
// starInfo: {
//   starMass: number (ratio to earth)
//   starLum: number (ratio to earth)
//   starRad: number (ratio to earth)
//   starTemp: number (in Kelvin)
//   starHabMod: integer (no unit, just a modifier for habitability)
//   minLifeDist:  number (in AU)
//   maxLifeDist: number (in AU)
// }
// planetInfo: {
//   orbitalSlot: integer
//   orbitalDistance: number (in AU)
// }
// overrides: {
//   planetType: integer 0..5 (see planet type table)
//   planetDiameter: number (in KM)
//   density: number (in g/cm^3)
//   hasRings: boolean
//   moons: array<Moon>
//   habitable: boolean
//   percentSurfaceWater: number (20% would be just 20, not 0.20)
//   atmoCondition: integer (see atmospheric condition table)
//   planetTemperature: number (in Kelvin)
//}
function generatePlanetInfo(seed, starInfo, planetInfo, overrides) {
  overrides = overrides || {};
  if (!seed) {
    seed = (Math.random() * 1000000000) | 0;
  }

  const rng = rngFactory(seed);

  // helper internal function for getting a set number of rolls
  function getNextRolls(quantity) {
    return rollnD6(rng, quantity);
  }

  // TODO: prevent gas giants inside the max habitable radius
  // prevent ice giants inside the max habitable radius * some multiplier
  let planetType = null;
  let maxAttempts = 10;
  const lifeZoneSize = starInfo.maxLifeDist - starInfo.minLifeDist;
  const minGasGiantDist = starInfo.maxLifeDist;
  const minIceGiantDist = starInfo.maxLifeDist * 1.5;

  let maxPlanetTypeIndex = planetTypeTable.length;
  if (planetInfo.orbitalDistance < minGasGiantDist) {
    maxPlanetTypeIndex = 6;
  } else if (planetInfo.orbitalDistance < minIceGiantDist) {
    maxPlanetTypeIndex = 8;
  }

  do {
    const planetTypeRoll = getNextRolls(2);
    const planetTypeIndex = overrideOrValue(overrides.planetType, (planetTypeRoll - 2));

    if (planetTypeIndex <= maxPlanetTypeIndex) {
      planetType = planetTypeTable[planetTypeIndex];
    }
  } while (planetType == null);

  if (planetType.name === 'Empty') {
    return Object.assign({planetType: 'Empty'}, planetInfo);
  }

  // this will effectively shallow-clone the planetInfo passed in
  planetInfo = Object.assign({planetType: planetType.name}, planetInfo);

  // safe to use planetInfo as the destination, since we are using our own shallow-clone
  planetInfo = Object.assign(planetInfo, planetType.generate(starInfo, planetInfo, getNextRolls, overrides));

  if (planetInfo.habitable) {
    const terrainSeedBuffer = Buffer.from([
      0xDA,
      0x20,

      0xB3,
      0x3F
    ]);

    const terrainSeed = xxhash(terrainSeedBuffer, seed).toNumber();
    planetInfo.terrainSeed = terrainSeed;
  }

  return planetInfo;
}

module.exports = generatePlanetInfo;

'use strict';

const overrideOrValue = require('../../utils/override-or-value');

const generateVacuumAtmo = require('../utils/generate-vacuum-atmo');
const generateMoon = require('../utils/generate-moon');

module.exports = {
  name: 'Gas Giant',

  generate: function(starInfo, planetInfo, getNextRolls, overrides) {
    overrides = overrides || {};

    const baseDiameter = 50000;
    const diameterModifier = (getNextRolls(2)) * 10000;
    const planetDiameter = overrideOrValue(overrides.diameter, (baseDiameter + diameterModifier));

    const density = overrideOrValue(overrides.density, (0.5 + getNextRolls(2) / 10));
    const gravityPerTerra = (planetDiameter / 12742.0) * (density / 5.5153);
    const escapeVelocity = (planetDiameter / 12742) * Math.sqrt(density / 5.5153) * 11186;
    const escapeVelocityRatio = (escapeVelocity / 11186.0);

    const yearLength = Math.sqrt(Math.pow(planetInfo.orbitalDistance, 3) / starInfo.starMass);
    const dayLength = getNextRolls(4);

    const moonRoll = Math.floor((getNextRolls(1) - 1) / 2);
    let moons = [];
    let hasRings = false;

    if (moonRoll === 0) {
      const giantMoons = Math.max(0, (getNextRolls(1) - 4));
      const largeMoons = Math.max(0, (getNextRolls(1) - 1));
      const mediumMoons = Math.max(0, (getNextRolls(1) - 2));
      const smallMoons = Math.max(0, (getNextRolls(5)));
      hasRings = getNextRolls(1) >= 4;

      for (let i = 0; i < smallMoons; i++) { moons.push('Small'); }
      for (let i = 0; i < mediumMoons; i++) { moons.push('Medium'); }
      for (let i = 0; i < largeMoons; i++) { moons.push('Large'); }
      for (let i = 0; i < giantMoons; i++) { moons.push('Giant'); }
    } else if (moonRoll === 1) {
      const giantMoons = 0;
      const largeMoons = Math.max(0, (getNextRolls(1) - 3));
      const mediumMoons = Math.max(0, (getNextRolls(1) - 2));
      const smallMoons = Math.max(0, (getNextRolls(5)));
      hasRings = getNextRolls(1) >= 3;

      for (let i = 0; i < smallMoons; i++) { moons.push('Small'); }
      for (let i = 0; i < mediumMoons; i++) { moons.push('Medium'); }
      for (let i = 0; i < largeMoons; i++) { moons.push('Large'); }
      for (let i = 0; i < giantMoons; i++) { moons.push('Giant'); }
    } else if (moonRoll === 2) {
      const giantMoons = 0;
      const largeMoons = Math.max(0, (getNextRolls(1) - 4));
      const mediumMoons = Math.max(0, (getNextRolls(1) - 3));
      const smallMoons = Math.max(0, (getNextRolls(5)));
      hasRings = getNextRolls(1) >= 3;

      for (let i = 0; i < smallMoons; i++) { moons.push('Small'); }
      for (let i = 0; i < mediumMoons; i++) { moons.push('Medium'); }
      for (let i = 0; i < largeMoons; i++) { moons.push('Large'); }
      for (let i = 0; i < giantMoons; i++) { moons.push('Giant'); }
    }

    const atmosphericInfo = generateVacuumAtmo(getNextRolls, starInfo, planetInfo, overrides);

    return Object.assign({
      planetDiameter: planetDiameter, // in KM
      density: density, // density
      gravityPerTerra: gravityPerTerra, // gravity in Earth-gravity units (so 1 = 9.8 m/s, 2 = 19.6 m/s)
      escapeVelocity: escapeVelocity, // escape velocity in KM/s
      escapeVelocityRatio: escapeVelocityRatio, // ratio of escape velocity to Earth's escape velocity
      yearLength: yearLength, // in Earth-years
      dayLength: dayLength, // in Earth-hours

      moons: moons.map(type => generateMoon(type, starInfo, planetInfo, getNextRolls)),
      hasRings: overrideOrValue(overrides.hasRings, hasRings),

      habitable: false
    }, atmosphericInfo);
  }
};

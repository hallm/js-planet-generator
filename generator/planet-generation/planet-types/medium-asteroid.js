'use strict';

const overrideOrValue = require('../../utils/override-or-value');

const generateVacuumAtmo = require('../utils/generate-vacuum-atmo');

module.exports = {
  name: 'Medium Asteroid',

  generate: function(starInfo, getNextRolls, overrides) {
    overrides = overrides || {};

    const planetDiameter = overrideOrValue(overrides.diameter, (getNextRolls(1)) * 100);

    const density = Math.pow(getNextRolls(1), 1.15);
    const gravityPerTerra = (planetDiameter / 12742.0) * (density / 5.5153);
    const escapeVelocity = (planetDiameter / 12742) * Math.sqrt(density / 5.5153) * 11186;
    const escapeVelocityRatio = (escapeVelocity / 11186.0);

    const dayLength = getNextRolls(2);

    const atmosphericInfo = generateVacuumAtmo(getNextRolls, starInfo, {}, overrides);

    return Object.assign({
      planetDiameter: planetDiameter, // in KM
      density: density, // density
      gravityPerTerra: gravityPerTerra, // gravity in Earth-gravity units (so 1 = 9.8 m/s, 2 = 19.6 m/s)
      escapeVelocity: escapeVelocity, // escape velocity in KM/s
      escapeVelocityRatio: escapeVelocityRatio, // ratio of escape velocity to Earth's escape velocity
      dayLength: dayLength,

      habitable: false
    }, atmosphericInfo);
  }
};

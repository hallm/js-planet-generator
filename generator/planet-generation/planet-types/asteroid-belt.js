'use strict';

const overrideOrValue = require('../../utils/override-or-value');

const dwarfGenerator = require('./dwarf-asteroid');

module.exports = {
  name: 'Asteroid Belt',

  generate: function(starInfo, planetInfo, getNextRolls, overrides) {
    overrides = overrides || {};

    const density = overrideOrValue(overrides.density, (Math.pow(getNextRolls(1), 1.15)));
    const yearLength = Math.sqrt(Math.pow(planetInfo.orbitalDistance, 3) / starInfo.starMass);

    const populationModifier =
      (planetInfo.orbitalDistance / 2.8)
      * (Math.pow(getNextRolls(1) / 3.0), 2);

    const dwarfsCount = Math.round(4 * populationModifier);
    const mediumsCount = Math.round(200 * populationModifier);
    const smallsCount = Math.round(1200000 * populationModifier);

    const dwarfs = new Array(dwarfsCount);
    for (let i = 0; i < dwarfsCount; i++) {
      dwarfs[i] = Object.assign({
        planetType: 'Dwarf Asteroid'
      }, dwarfGenerator.generate(starInfo, getNextRolls, overrides));
    }

    // TODO: randomly decide if we should generate "interesting" medium or small asteroids

    return {
      habitable: false,

      asteroids: dwarfs
    };
  }
};

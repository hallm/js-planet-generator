'use strict';

const overrideOrValue = require('../../utils/override-or-value');

const atmoPressureTable = require('../tables/atmo-pressure');

const generateHabitableAtmo = require('../utils/generate-habitable-atmo');
const generateUninhabitableAtmo = require('../utils/generate-uninhabitable-atmo');
const generateVacuumAtmo = require('../utils/generate-vacuum-atmo');

module.exports = {
  name: 'Giant Asteroid',

  generate: function(starInfo, planetInfo, getNextRolls, overrides) {
    overrides = overrides || {};

    const baseDiameter = 2500;
    const diameterModifier = (getNextRolls(2)) * 1000;
    const planetDiameter = overrideOrValue(overrides.diameter, (baseDiameter + diameterModifier));

    const density = overrideOrValue(overrides.density, (2.5 + (Math.pow(getNextRolls(1), 0.75))));
    const gravityPerTerra = (planetDiameter / 12742.0) * (density / 5.5153);
    const escapeVelocity = (planetDiameter / 12742) * Math.sqrt(density / 5.5153) * 11186;
    const escapeVelocityRatio = (escapeVelocity / 11186.0);

    const dayLength = getNextRolls(3) + 12.0;

    const atmoPressureRoll = getNextRolls(2) - (planetInfo.orbitalDistance < starInfo.minLifeDist ? 2 : 0);
    const atmoPressureIndex = Math.max(2, Math.min(12, Math.round(atmoPressureRoll * escapeVelocityRatio)));
    const atmospherePressure = atmoPressureTable[atmoPressureIndex - 2];

    const habitable = false;

    let atmoGenerator;
    if (atmoPressureIndex <= 4) {
      atmoGenerator = generateVacuumAtmo;
    } else {
      atmoGenerator = generateUninhabitableAtmo;
    }

    const atmosphericInfo = atmoGenerator(getNextRolls, starInfo, planetInfo, overrides);

    return Object.assign({
      planetDiameter: planetDiameter, // in KM
      density: density, // density
      gravityPerTerra: gravityPerTerra, // gravity in Earth-gravity units (so 1 = 9.8 m/s, 2 = 19.6 m/s)
      escapeVelocity: escapeVelocity, // escape velocity in KM/s
      escapeVelocityRatio: escapeVelocityRatio, // ratio of escape velocity to Earth's escape velocity
      dayLength: dayLength, // in Earth-hours

      habitable: habitable,
      atmospherePressure: atmospherePressure
    }, atmosphericInfo);
  }
};

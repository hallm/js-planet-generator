'use strict';

module.exports = [
  {name: 'Empty'},
  {name: 'Empty'},
  require('../planet-types/asteroid-belt'),
  require('../planet-types/dwarf-terrestrial'),
  require('../planet-types/terrestrial'),
  require('../planet-types/terrestrial'),
  require('../planet-types/giant-terrestrial'),
  require('../planet-types/gas-giant'),
  require('../planet-types/gas-giant'),
  require('../planet-types/ice-giant'),
  require('../planet-types/ice-giant')
];

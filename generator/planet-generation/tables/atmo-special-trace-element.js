'use strict';

module.exports = [
  'Helium',
  'Complex Hydrocarbons',
  'Nitric Acid',
  'Phosphine',
  'Hydrogen Peroxide',
  'Hydrochloric Acid',
  'Hydrogen Sulfide',
  'Simple Hydrocarbons',
  'Sulfuric Acid',
  'Carbonyl Sulfide',
  'Hydrofluoric Acid'
];

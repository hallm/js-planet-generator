'use strict';

module.exports = [
  'Chlorine',
  'Sulfur Dioxide',
  'Nitric Acid',
  'Hydrogen Peroxide',
  'Hydrochloric Acid',
  'Sulfuric Acid',
  'Hydrofluoric Acid'
];

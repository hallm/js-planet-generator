'use strict';

const giantAsteroidGenerator = require('../planet-types/giant-asteroid');
const dwarfAsteroidGenerator = require('../planet-types/dwarf-asteroid');
const mediumAsteroidGenerator = require('../planet-types/medium-asteroid');
const smallAsteroidGenerator = require('../planet-types/small-asteroid');

module.exports = function generateMoon(moonType, starInfo, planetInfo, getNextRolls) {
  let moonInfo;

  switch (moonType) {
    case 'Giant':
      const moonInfo = {
        type: 'Giant Moon',
        orbitalDistance: planetInfo.orbitalDistance
      };
      return Object.assign(moonInfo, giantAsteroidGenerator.generate(starInfo, moonInfo, getNextRolls));
      break;
    case 'Large':
      return Object.assign({
        type: 'Small Moon'
      }, dwarfAsteroidGenerator.generate(starInfo, getNextRolls));
      break;
    case 'Medium':
      return Object.assign({
        type: 'Small Moon'
      }, mediumAsteroidGenerator.generate(starInfo, getNextRolls));
      break;
    case 'Small':
      return Object.assign({
        type: 'Small Moon'
      }, smallAsteroidGenerator.generate(starInfo, getNextRolls));
      break;
  }

  return {type: 'Empty'};
};

'use strict';

const overrideOrValue = require('../../utils/override-or-value');

module.exports = function generateVacuumAtmo(getNextRolls, starInfo, planetInfo, overrides) {
  // TODO: get NANs for moon
  const planetTemperature = overrideOrValue(
    overrides.planetTemperature,
    277.0 * Math.pow(starInfo.starLum, 0.25) * Math.sqrt(1.0 / planetInfo.orbitalDistance)
  );

  return {
    planetTemperature: planetTemperature
  };
};

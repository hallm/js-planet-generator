'use strict';

const atmoConditionTable = require('../tables/habitable-atmo-conditions');
const temperatureTable = require('../tables/habitable-temperature');
const percentSurfaceWaterTable = require('../tables/percent-surface-water');
const overrideOrValue = require('../../utils/override-or-value');

module.exports = function generateHabitableAtmo(getNextRolls, starInfo, planetInfo, overrides) {
  const isGiantTerrestrial = planetInfo.planetType === 'Giant Terrestrial';

  const lifeZoneModifier =
    (planetInfo.orbitalDistance - starInfo.minLifeDist)
    / (starInfo.maxLifeDist - starInfo.minLifeDist);

  const waterRoll = getNextRolls(2);
  const waterIndex = Math.round(waterRoll * lifeZoneModifier * planetInfo.escapeVelocityRatio) + (isGiantTerrestrial ? 3 : 0);
  const percentSurfaceWater = overrideOrValue(
    overrides.percentSurfaceWater,
    waterIndex < 0 ? percentSurfaceWaterTable[0] : percentSurfaceWaterTable[Math.min(waterIndex, 12) + 1]
  );

  const atmoConditionRoll = overrideOrValue(
    overrides.atmoCondition,
    getNextRolls(2) - (isGiantTerrestrial ? 2 : 0)
  );
  const atmoCondition = atmoConditionRoll < 0 ? atmoConditionTable[0] : atmoConditionTable[Math.min(atmoConditionRoll, 12) + 1];

  let temperaturePressureModifier = 0;
  if (planetInfo.atmospherePressure === 'Low') {
    temperaturePressureModifier++; // more likely to be colder
  } else if (planetInfo.atmospherePressure === 'High') {
    temperaturePressureModifier--; // more likely to be hotter
  }

  const lifeZoneSize = starInfo.maxLifeDist - starInfo.minLifeDist;
  const lifeZoneFirstQuarter = starInfo.minLifeDist + (lifeZoneSize / 4);
  const lifeZoneThirdQuarter = starInfo.maxLifeDist - (lifeZoneSize / 4);

  if (planetInfo.orbitalDistance < lifeZoneFirstQuarter) {
    temperaturePressureModifier--; // more likely to be hotter
  } else if (planetInfo.orbitalDistance < lifeZoneThirdQuarter) {
    temperaturePressureModifier++; // more likely to be colder
  }

  const temperatureRoll = getNextRolls(2) + (isGiantTerrestrial ? 3 : 0) + temperaturePressureModifier;
  let temperatureIndex = Math.round(temperatureRoll * lifeZoneModifier);
  if (atmoCondition === 'Low') {
    temperatureIndex += 1;
  } else if (atmoCondition === 'High') {
    temperatureIndex -= 1;
  }

  const temperatureStr = temperatureRoll < 0 ? temperatureTable[0] : temperatureTable[Math.min(temperatureRoll, 12) + 1];

  let planetTemperature;
  switch (temperatureStr) {
    case 'Very High':
      planetTemperature = 317.00;
      break;
    case 'High':
      planetTemperature = 307.00;
      break;
    case 'Medium':
      planetTemperature = 297.00;
      break;
    case 'Low':
      planetTemperature = 287.00;
      break;
  }

  planetTemperature = overrideOrValue(overrides.planetTemperature, planetTemperature);

  return {
    planetTemperature: planetTemperature,
    percentSurfaceWater: percentSurfaceWater,
    atmoCondition: atmoCondition
  };

}
'use strict';

const overrideOrValue = require('../../utils/override-or-value');

const causticChemicals = require('../tables/caustic-chemicals');
const baseCompositionTable = require('../tables/atmo-base-element');
const secondaryCompositionTable = require('../tables/atmo-secondary-element');
const traceCompositionTable = require('../tables/atmo-trace-element');
const specialTraceCompositionTable = require('../tables/atmo-special-trace-element');

module.exports = function generateUninhabitableAtmo(getNextRolls, starInfo, planetInfo, overrides) {
  const planetTemperature = overrideOrValue(
    overrides.planetTemperature,
    277.0 * Math.pow(starInfo.starLum, 0.25) * Math.sqrt(1.0 / planetInfo.orbitalDistance)
  );

  let atmosphericCompositionModifier = 0;
  if (planetInfo.orbitalDistance > starInfo.maxLifeDist) {
    atmosphericCompositionModifier -= 2;
  } else if (planetInfo.orbitalDistance < starInfo.minLifeDist) {
    atmosphericCompositionModifier += 2;
  }

  if (planetInfo.escapeVelocity > 12000) {
    atmosphericCompositionModifier -= 1;
  } else if (planetInfo.escapeVelocity < 7000) {
    atmosphericCompositionModifier += 1;
  }

  // bound the rolls between 2 and 12 first, then subtract 2 to match the tables
  const baseAtmoCompRoll = Math.max(2, Math.min(12, getNextRolls(2) + atmosphericCompositionModifier)) - 2;
  const secondaryAtmoCompRoll = Math.max(2, Math.min(12, getNextRolls(2) + atmosphericCompositionModifier)) - 2;

  // the traces do not get the modifier
  let traceAtmoCompRoll = Math.max(2, Math.min(12, getNextRolls(2))) - 2;
  if (traceAtmoCompRoll === 9) {
    // Roll again twice on this column, ignoring results of 11 or 12+
    const firstExtraRoll = Math.max(2, Math.min(12, getNextRolls(2))) - 2;
    const secondExtraRoll = Math.max(2, Math.min(12, getNextRolls(2))) - 2;

    traceAtmoCompRoll = firstExtraRoll < 9 ? firstExtraRoll : secondExtraRoll;
  } else if (traceAtmoCompRoll === 10) {
    // Roll again once on this column (ignoring 11 or 12) and once on the Special Traces Table
    traceAtmoCompRoll = Math.max(2, Math.min(12, getNextRolls(2))) - 2;
  }

  const specialTraceAtmoCompRoll = Math.max(2, Math.min(12, getNextRolls(2))) - 2;

  // we get these in backwards order
  const specialTraceAtmoCompPercent = getNextRolls(1) / 2.0;
  const traceAtmoCompPercent = getNextRolls(1) / 2.0;
  const secondaryAtmoCompPercent = getNextRolls(5);
  const baseAtmoCompPercent =
    100
    - secondaryAtmoCompPercent
    - traceAtmoCompPercent
    - specialTraceAtmoCompPercent
  ;

  return {
    planetTemperature: planetTemperature,

    composition: {
      base: {
        chem: baseCompositionTable[baseAtmoCompRoll],
        percent: baseAtmoCompPercent
      },
      secondary: {
        chem: secondaryCompositionTable[secondaryAtmoCompRoll],
        percent: secondaryAtmoCompPercent
      },
      trace: {
        chem: traceCompositionTable[traceAtmoCompRoll],
        percent: traceAtmoCompPercent
      },
      specialTrace: {
        chem: specialTraceCompositionTable[specialTraceAtmoCompRoll],
        percent: specialTraceAtmoCompPercent
      }
    },

    isCaustic:
      isChemicalCaustic(traceCompositionTable[traceAtmoCompRoll])
      || isChemicalCaustic(specialTraceCompositionTable[specialTraceAtmoCompRoll])
  };
};

function isChemicalCaustic(chemical) {
  return causticChemicals.indexOf(causticChemicals) !== -1;
}

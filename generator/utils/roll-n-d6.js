
// returns the sum of n number of D6 rolls
// doing each roll individually as it may alter the distribution of the randomness
// so 2D6 may be different than randomInt(2, 12)
module.exports = function rollnD6(rng, n) {
  n = n || 1;

  let sum = 0;
  for (let i = 0; i < n; i++) {
    sum += (1 + Math.floor(rng() * 6));
  }

  return sum;
}

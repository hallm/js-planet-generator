// http://devmag.org.za/2009/05/03/poisson-disk-sampling/

const Grid = require('./grid');
const RandomQueue = require('../../utils/random-queue');

function defaultRngFn() {
  return Math.random();
}

function defaultDistFn(point1, point2) {
  const sx = point1.x - point2.x;
  const sy = point1.y - point2.y;
  return Math.sqrt((sx * sx) + (sy * sy));
}

function Point(x, y) {
  this.x = x;
  this.y = y;
}

function Poisson(width, height, rngFn, distFn) {
  if (!width || typeof width !== 'number' || width <= 0) {
    throw new TypeError('width must be a set to a positive number');
  }
  if (!height || typeof height !== 'number' || height <= 0) {
    throw new TypeError('height must be a set to a positive number');
  }

  this.rngFn = rngFn || defaultRngFn;
  this.distFn = distFn || defaultDistFn;
  this.width = width;
  this.height = height;
}

Poisson.prototype.generatePoisson = function generatePoisson(minDist, numOfNeighborsPerPoint) {
  //Create the grid
  const cellSize = minDist / Math.sqrt(2);

  const grid = new Grid(this.width, this.height, cellSize);

  //RandomQueue works like a queue, except that it
  //pops a random element from the queue instead of
  //the element at the head of the queue
  const processList = new RandomQueue(this.rngFn);
  const samplePoints = [];

  //generate the first point randomly
  //and updates
  const firstPoint = new Point(
    this.rngFn() * this.width,
    this.rngFn() * this.height
  );

  //update containers
  processList.push(firstPoint);
  samplePoints.push(firstPoint);
  grid.setAtPoint(firstPoint, firstPoint);

  //generate other points from points in queue.
  while (!processList.isEmpty()) {
    const point = processList.pop();

    for (let i = 0; i < numOfNeighborsPerPoint; i++) {
      const newPoint = this.generateRandomPointAround(point, minDist);

      //check that the point is in the image region
      //and no points exists in the point's neighbourhood
      if (this.inRectangle(newPoint) && !this.inNeighbourhood(grid, newPoint, minDist)) {
        //update containers
        processList.push(newPoint);
        samplePoints.push(newPoint);
        grid.setAtPoint(newPoint, newPoint);
      }
    }
  }

  return samplePoints;
};

Poisson.prototype.inRectangle = function inRectangle(point) {
  return point.x >= 0 && point.y >= 0 && point.x < this.width && point.y < this.height;
};

//non-uniform, favours points closer to the inner ring, leads to denser packings
Poisson.prototype.generateRandomPointAround = function generateRandomPointAround(point, minDist) {
  const r1 = this.rngFn();
  const r2 = this.rngFn();

  //random radius between mindist and 2 * mindist
  const radius = minDist * (r1 + 1);

  //random angle
  const angle = 2 * Math.PI * r2;

  //the new point is generated around the point (x, y)
  const newX = point.x + radius * Math.cos(angle);
  const newY = point.y + radius * Math.sin(angle);

  return new Point(newX, newY);
};

Poisson.prototype.inNeighbourhood = function inNeighbourhood(grid, point, minDist) {
  const cellValues = grid.getValuesAroundPoint(point);
  return cellValues.some(value => value != null && this.distFn(point, value) < minDist);
};

module.exports = Poisson;

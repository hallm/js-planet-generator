function Grid(width, height, cellSize) {
  this.width = width;
  this.height = height;
  this.cellSize = cellSize;

  this.numOfCols = Math.ceil(width / cellSize);
  this.numOfRows = Math.ceil(height / cellSize);

  const numOfCells = this.numOfCols * this.numOfRows;

  this.grid = new Array(numOfCells);

  for (let i = 0; i < numOfCells; i++) {
    this.grid[i] = [];
  }
}

Grid.prototype.indexOfPoint = function indexOfPoint(point) {
  const gridX = Math.floor(point.x / this.cellSize);
  const gridY = Math.floor(point.y / this.cellSize);

  const index = gridY * this.numOfCols + gridX;
  return index;
};

Grid.prototype.setAtPoint = function setAtPoint(point, value) {
  const index = this.indexOfPoint(point);
  this.grid[index].push(value);
  // this.grid[index] = value;
};

Grid.prototype.getValuesAroundPoint = function getValuesAroundPoint(point) {
  const gridX = Math.floor(point.x / this.cellSize);
  const gridY = Math.floor(point.y / this.cellSize);

  let cellValues = [];

  const colBefore = gridX - 1;
  const colAfter = gridX + 1;
  const rowBefore = gridY - 1;
  const rowAfter = gridY + 1;

  // add the row above if it exists
  if (gridY > 0) {
    if (gridX > 0) {
      cellValues = cellValues.concat(this.grid[rowBefore * this.numOfCols + colBefore]);
    }

    cellValues = cellValues.concat(this.grid[rowBefore * this.numOfCols + gridX]);

    if (colAfter < this.numOfCols) {
      cellValues = cellValues.concat(this.grid[rowBefore * this.numOfCols + colAfter]);
    }
  }

  // add the row of cells
  if (gridX > 0) {
    cellValues = cellValues.concat(this.grid[gridY * this.numOfCols + colBefore]);
  }

  cellValues = cellValues.concat(this.grid[gridY * this.numOfCols + gridX]);

  if (colAfter < this.numOfCols) {
    cellValues = cellValues.concat(this.grid[gridY * this.numOfCols + colAfter]);
  }

  // add the row below if it exists
  if (rowAfter < this.numOfRows) {
    if (gridX > 0) {
      cellValues = cellValues.concat(this.grid[rowAfter * this.numOfCols + colBefore]);
    }

    cellValues = cellValues.concat(this.grid[rowAfter * this.numOfCols + gridX]);

    if (colAfter < this.numOfCols) {
      cellValues = cellValues.concat(this.grid[rowAfter * this.numOfCols + colAfter]);
    }
  }

  return cellValues;
};

module.exports = Grid;

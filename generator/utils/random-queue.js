function RandomQueue(rngFn) {
  if (!rngFn || typeof rngFn !== 'function') {
    throw new TypeError('rngFn must be a function which generates random floating point number from 0 to 1');
  }

  this.rngFn = rngFn;
  this.values = [];
}

RandomQueue.prototype.push = function push(value) {
  this.values.push(value);
};

RandomQueue.prototype.pop = function pop() {
  const randomIndex = Math.floor(this.rngFn() * this.values.length);
  const value = this.values[randomIndex];

  this.values.splice(randomIndex, 1);

  return value;
};

RandomQueue.prototype.isEmpty = function isEmpty() {
  return !this.values.length;
};

module.exports = RandomQueue;

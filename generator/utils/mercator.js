const RAD2DEG = 180 / Math.PI;
const PI_4 = Math.PI / 4;

function Mercator(planetRadiusInKm, mapWidth, mapHeight) {
  this.kmPerUnitWidth = (planetRadiusInKm * 2.0 * Math.PI) / mapWidth;
  this.planetRadiusInKm = planetRadiusInKm;
  this.mapWidth = mapWidth;
  this.mapHeight = mapHeight;
}

Mercator.prototype.project = function project(point) {
  const x = (point.x / this.mapWidth * 360.0) - 180;
  const y = 180.0 - (point.y * 360.0 / this.mapHeight);

  return {
    lng: x2lon(x),
    lat: y2lat(y)
  };
};

Mercator.prototype.unproject = function unproject(coord) {
  const x = lon2x(coord.lng);
  const y = lat2y(coord.lat);

  return {
    x: (x + 180.0) * this.mapWidth / 360.0,
    y: (180 - y) * this.mapHeight / 360.0
  };
};

// http://wiki.openstreetmap.org/wiki/Mercator
function y2lat(y) {
  return (Math.atan(Math.exp(y / RAD2DEG)) / PI_4 - 1) * 90;
}
function x2lon(x) {
  return x;
}
function lat2y(lat) {
  return Math.log(Math.tan((lat / 90 + 1) * PI_4 )) * RAD2DEG;
}
function lon2x(lon) {
  return lon;
}

Mercator.prototype.distance = function distance(p1m, p2m) {
  const p1 = this.project(p1m);
  const p2 = this.project(p2m);

  return this.coordDistance(p1, p2) / this.kmPerUnitWidth;
};

Mercator.prototype.coordDistance = function coordDistance(p1, p2) {
  const dLat = (p2.lat - p1.lat) / RAD2DEG;
  const dLon = (p2.lng - p1.lng) / RAD2DEG;

  const sinHalfLat = Math.sin(dLat / 2.0);
  const sinHalfLng = Math.sin(dLon / 2.0);

  const a = sinHalfLat * sinHalfLat +
    Math.cos(p1.lat / RAD2DEG) * Math.cos(p2.lat / RAD2DEG) *
    sinHalfLng * sinHalfLng;

  const c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1.0 - a));

  return this.planetRadiusInKm * c;
};

// returns an angle in radians
// reference: http://www.movable-type.co.uk/scripts/latlong.html
Mercator.prototype.angleBetweenPoints = function angleBetweenPoints(p1, p2) {
  const lat1 = p1.lat / RAD2DEG;
  const lat2 = p2.lat / RAD2DEG;
  const lng1 = p1.lng / RAD2DEG;
  const lng2 = p2.lng / RAD2DEG;

  const dLng = lng2 - lng1;

  const y = Math.sin(dLng) * Math.cos(lat2);
  const x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLng);

  return Math.atan2(y, x);
};

Mercator.prototype.pointFrom = function pointFrom(coord, headingRad, distanceKm) {
  const distRadiusRatio = distanceKm / this.planetRadiusInKm;

  const lat1Rad = coord.lat / RAD2DEG;
  const lng1Rad = coord.lng / RAD2DEG;

  const lat2Rad = Math.asin(
    Math.sin(lat1Rad) * Math.cos(distRadiusRatio) +
    Math.cos(lat1Rad) * Math.sin(distRadiusRatio) * Math.cos(headingRad)
  );

  const lon2Rad = lng1Rad +
    Math.atan2(
      Math.sin(headingRad) * Math.sin(distRadiusRatio) * Math.cos(lat1Rad),
      Math.cos(distRadiusRatio) - Math.sin(lat1Rad) * Math.sin(lat2Rad)
    );

  const lat = lat2Rad * RAD2DEG;
  let lng = lon2Rad * RAD2DEG;

  if (lng > 180) {
    lng -= 360.0;
  } else if (lng <= -180) {
    lng += 360.0;
  }

  return {
    lat: lat,
    lng: lng
  };
};


module.exports = Mercator;

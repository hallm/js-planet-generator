'use strict';

// module.exports = require('seedrandom');

const MersenneTwister = require('mersenne-twister');

module.exports = function rngFactory(seed) {
  const rng = new MersenneTwister(seed);
  return rng.random.bind(rng);
};

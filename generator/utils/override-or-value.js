'use strict';

module.exports = function overrideOrValue(override, value) {
  if (override != null) {
    return override;
  }

  return value;
}

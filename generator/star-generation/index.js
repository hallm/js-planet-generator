'use strict';

const rngFactory = require('../utils/rng-factory');
const overrideOrValue = require('../utils/override-or-value');
const rollnD6 = require('../utils/roll-n-d6');

// ----------------------------------------------------------------------------
// look up tables
// ----------------------------------------------------------------------------
const starTypeTable = require('./tables/life-friendly-star-types');
const starSubTypeTable = require('./tables/star-sub-types');
const starInfoTypeTable = require('./tables/star-info');
const orbitalSlotsTable = require('./tables/orbital-slots');

// ----------------------------------------------------------------------------
// Generation function
// ----------------------------------------------------------------------------
//
// inputs:
// rng: Function() -> number[0..1]
// overrides: {
//   starType: integer (see star type table)
//   starSubType: integer (see star sub-type table)
//   orbitalSlotCount: integer (0 to 15)
//}
function generateStarInfo(seed, overrides) {
  overrides = overrides || {};
  if (!seed) {
    seed = (Math.random() * 1000000000) | 0;
  }

  const rng = rngFactory(seed);

  // helper internal function for getting a set number of rolls
  function getNextRolls(quantity) {
    return rollnD6(rng, quantity);
  }

  const starTypeRoll = getNextRolls(2);
  const starSubTypeRoll = getNextRolls(1);
  const orbitalSlotRoll = getNextRolls(2);

  const starType = overrideOrValue(overrides.starType, starTypeTable[starTypeRoll - 2]);
  const starSubType = overrideOrValue(overrides.starSubType, starSubTypeTable[starSubTypeRoll - 1]);
  const orbitalSlotCount = overrideOrValue(overrides.orbitalSlotCount, orbitalSlotRoll + 3);

  const starInfo = starInfoTypeTable[starType + starSubType + 'V'];

  // get the orbit distances and convert from Sol-relative distances to star's distances
  const orbitalSlots = orbitalSlotsTable
    .slice(0, orbitalSlotCount)
    .map(orbitalDist => orbitalDist * starInfo.starMass);

  return Object.assign({orbitalSlots: orbitalSlots}, starInfo);
}

module.exports = generateStarInfo;

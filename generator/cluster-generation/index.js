'use strict';

const xxhash = require('xxhashjs').h32;
const Poisson = require('../utils/poisson/');
const rngFactory = require('../utils/rng-factory');

const generateSystemInfo = require('../system-generation/');

const clusterWidth = 900;
const clusterHeight = 900;

const minDistance = 30;
const generatePerPoint = 18;

// takes a seed for generation
// use poisson to generate a bunch of points in a map
// then uses that seed as a seed for xxhash to hash a coordinate
// the 32-bit xxhash of that is used for generating the star system at that coordinate

function generateClusterInfo(seed, shouldGenerateSystems) {
  if (!seed) {
    seed = (Math.random() * 1000000000) | 0;
  }
  const rng = rngFactory(seed);

  const generator = new Poisson(clusterWidth, clusterHeight, rng);

  const systemCoordinates = generator.generatePoisson(
    minDistance,
    generatePerPoint
  );

  return systemCoordinates.map(function(coordinate, index) {
    const systemCoordBuffer = Buffer.from([
      ((coordinate.x >> 8) & 0xFF),
      (coordinate.x & 0xFF),

      ((coordinate.y >> 8) & 0xFF),
      (coordinate.y & 0xFF)
    ]);

    const systemSeed = xxhash(systemCoordBuffer, seed).toNumber();

    const systemInfo = shouldGenerateSystems ? generateSystemInfo(systemSeed) : {};

    return Object.assign({
      _id: index,
      x: coordinate.x,
      y: coordinate.y,
      seed: systemSeed
    }, systemInfo);
  });
}

module.exports = generateClusterInfo;

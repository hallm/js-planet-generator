'use strict';

module.exports = function determineNeighbors(rng, starInfo, planetInfo, mapInfo, idToCell, cells) {
  const southPoleCells = [];
  const northPoleCells = [];

  cells.forEach(function(cell) {
    const _id = cell._id;
    const point = cell.site;

    cell._isOnSouth = false;
    cell._isOnNorth = false;

    cell._neighbors = cell.halfedges
      .filter(he => he.edge.lSite && he.edge.rSite)
      .map(he => he.edge.lSite._id === cell.site._id ? he.edge.rSite._id : he.edge.lSite._id);

    // if it touches the bottom edge, then ALL that touch the bottom edge become neighbors
    // will add the neighbors later after processing all of them
    if (cell.halfedges.some(he => he.edge.vb.y >= mapInfo.mapHeight && he.edge.va.y >= mapInfo.mapHeight)) {
      southPoleCells.push(point._id);
      cell._isOnSouth = true;
    } else if (cell.halfedges.some(he => he.edge.vb.y <= 0.0 && he.edge.va.y <= 0.0)) {
      northPoleCells.push(point._id);
      cell._isOnNorth = true;
    }
  });

  console.log('poles determined');

  cells.forEach(function(cell) {
    const _id = cell._id;

    if (cell._isOnSouth) {
      cell._attachedNeighbors = cell._neighbors;
      cell._neighbors = cell._neighbors.concat(
        southPoleCells.filter(neighborId => neighborId !== _id && cell._neighbors.indexOf(neighborId) === -1)
      );
    } else if (cell._isOnNorth) {
      cell._attachedNeighbors = cell._neighbors;
      cell._neighbors = cell._neighbors.concat(
        northPoleCells.filter(neighborId => neighborId !== _id && cell._neighbors.indexOf(neighborId) === -1)
      );
    }
  });

  console.log('neighbors determined');
};

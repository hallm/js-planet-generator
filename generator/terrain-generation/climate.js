'use strict';

const distance = require('../utils/distance');

const maxMoistureGen = 20.0;
const minMoistureGen = 0.0;
const maxTempForMoistureGen = 300;
const minTempForMoistureGen = 260;
const tempRangeForMoistureGen = maxTempForMoistureGen - minTempForMoistureGen;
const tempToMoistureGen = tempRangeForMoistureGen / maxMoistureGen;

// the cellRetain is based on a 1 million sqkm area cell
// this detaches the climate model from cell counts and focuses on actual land mass
const baseCellRetain = 2.0;
const baseMountainCellRetain = 5.0;
const cellRetainMultipliers = [
  {maxTemperature: 270, multiplier: 1.56},
  {maxTemperature: 280, multiplier: 1.23},
  {maxTemperature: 290, multiplier: 1.1},
  {maxTemperature: 300, multiplier: 1.02}
];

// this is a percent of water that carries from one water cell to the next
// it helps prevent moisture from really building up before hitting land
const waterPersistencyMultiplier = 0.625;

const numberOfSimulations = 1000;

module.exports = function climateGeneration(rng, starInfo, planetInfo, mapInfo, idToCell, cells) {
  cells.forEach(function(cell) {
    // compute the wind direction, the starting moisture, the starting temp at point
    // for wind, split into 6 zones (0: 0->30, 1: 30->60, 2: 60->90, 3: 0->-30, ...)
    // 0, 3: <-- (neg)
    // 1, 4: --> (pos)
    // 2 is down (pos), slight <--
    // 5 is up (neg), slight <--
    // as approaching 0 from 30 or -30, it becomes more due <--, but is closer to 45deg start at 30
    // same effect when approaching 60/-60
    const cellWindZone = (cell.site.coord.lat / 30.0) | 0;
    if (cellWindZone === 0) {
      cell.windVector = {
        x: -1,
        y: Math.sin(cell.site.coord.lat * Math.PI / 180.0)
      };
    } else if (cellWindZone === 1) {
      cell.windVector = {
        x: 1,
        y: -Math.sin((60.0 - cell.site.coord.lat) * Math.PI / 180.0)
      };
    } else if (cellWindZone === -1) {
      cell.windVector = {
        x: 1,
        y: -Math.sin((-60.0 - cell.site.coord.lat) * Math.PI / 180.0)
      };
    } else if (cellWindZone === 2) {
      cell.windVector = {
        x: -0,
        y: 0.5
      };
    } else if (cellWindZone === -2) {
      cell.windVector = {
        x: 0,
        y: -0.5
      };
    }

    // for temp, 1 Kelvin drop per 3 degrees from equator
    // going to assume that the temperature doesn't need to be spread by wind
    // and that this rough estimate is "close enough"
    // TODO: some variation:
    // if it is a moutain, subtract some (based on height)
    // if it is a valley, add some (based on depth)
    // also, higher pressure systems have less temperature variance. low pressure have more temperature extremes
    let temperaturePerDeg = 0.3334;
    if (planetInfo.atmoCondition === 'Low') {
      temperaturePerDeg = 0.6667;
    } else if (planetInfo.atmoCondition === 'High') {
      temperaturePerDeg = 0.1250;
    }
    cell.temperature = planetInfo.planetTemperature - (Math.abs(cell.site.coord.lat) * temperaturePerDeg);

    // moisture stuff
    // "contains" is the the humidity of the cell. anything over "retains" will be moved to neighbors via wind
    // "retains" is how much moisture a cell absorbs and holds.
    // "totalTransfered" keeps track of how much moisture passed through this cell
    // temps can range from 257 (3 F) to 317 (111 F)
    let cellMoistureGeneration = 0;

    if (cell.isWater) {
      if (cell.temperature >= minTempForMoistureGen) {
        if (cell.temperature >= maxTempForMoistureGen) {
          cellMoistureGeneration = maxMoistureGen;
        } else {
          cellMoistureGeneration = (cell.temperature - minTempForMoistureGen) / tempToMoistureGen;
        }
      }
    }

    // cooler cells precipitate out more moisture than warmer cells
    // also, higher cells precipitate out more moisture
    // water doesnt retain, but it does have a "persistence" built into the simulation
    // TODO: compute retain based on area of the cell
    let cellMoistureRetain = 0.0;
    if (!cell.isWater) {
      if (cell._mountainHeight > 0) {
        cellMoistureRetain = baseMountainCellRetain;
      } else {
        cellMoistureRetain = baseCellRetain;
      }

      for (let i = 0; i < cellRetainMultipliers.length; i++) {
        const multiplierInfo = cellRetainMultipliers[i];
        if (cell.temperature < multiplierInfo.maxTemperature) {
          cellMoistureRetain *= multiplierInfo.multiplier;
          break;
        }
      }
    }

    // adjust the actual retain based on the cell area compared to 1 mil km
    // so a 500k km cell absorbs half. a 2m km cell absorbs double.
    const retainAreaMultiplier = cell.area / 1000000;
    cellMoistureRetain *= retainAreaMultiplier;

    cell.moisture = {
      contains: cellMoistureGeneration,
      retains: cellMoistureRetain,
      totalTransfered: 0
    };

    const windAngleRad = normalizeAngle(Math.atan2(cell.windVector.y, cell.windVector.x));
    // get the min/max angles to know if an edge is outside
    const minAngle = normalizeAngle(windAngleRad - (Math.PI / 2.0));
    const maxAngle = normalizeAngle(windAngleRad + (Math.PI / 2.0));

    // using the orthogLine to determine the percent of available moisture to transmit to adjacent cells
    const rhOrthogLineDirection = {
      x: cell.windVector.y,
      y: -cell.windVector.x
    };
    const lhOrthogLineDirection = {
      x: -cell.windVector.y,
      y: cell.windVector.x
    };

    // keep track of how long the proj sum is for ratio of water transfer
    let summedProjections = 0;

    // now need to compute the percent of stuff transfered via wind and to what cell
    const projectionInfo = cell.halfedges.map(function(he) {
      // check if the edge is covered within the wind. if either va or vb is inside the 180deg range, then good
      // now check if the edge intersects the orthog line. (either va or vb is outside). find the intersection and use that to the inside point
      // also, as we go, track the 2 points that will be intersections. their distance will be our width for the orthog line
      const vaAngle = normalizeAngle(Math.atan2(he.edge.va.y - cell.site.y, he.edge.va.x - cell.site.x));
      const vbAngle = normalizeAngle(Math.atan2(he.edge.vb.y - cell.site.y, he.edge.vb.x - cell.site.x));

      let isVaInside;
      let isVbInside;

      if (minAngle < maxAngle) {
        isVaInside = vaAngle > minAngle && vaAngle < maxAngle;
        isVbInside = vbAngle > minAngle && vbAngle < maxAngle;
      } else {
        isVaInside = (vaAngle > minAngle && vaAngle < (maxAngle + (Math.PI * 2.0))) || (vaAngle < maxAngle && vaAngle > (minAngle - (Math.PI * 2.0)))
        isVbInside = (vbAngle > minAngle && vbAngle < (maxAngle + (Math.PI * 2.0))) || (vbAngle < maxAngle && vbAngle > (minAngle - (Math.PI * 2.0)));
      }

      // since we dont know if va and vb are right orientation, try both
      const intersectionPoint =
        vector2dIntersection(
          he.edge.va,
          {
            x: he.edge.vb.x - he.edge.va.x,
            y: he.edge.vb.y - he.edge.va.y
          },
          cell.site,
          rhOrthogLineDirection
        ) || vector2dIntersection(
          he.edge.vb,
          {
            x: he.edge.va.x - he.edge.vb.x,
            y: he.edge.va.y - he.edge.vb.y
          },
          cell.site,
          rhOrthogLineDirection
        ) || vector2dIntersection(
          he.edge.va,
          {
            x: he.edge.vb.x - he.edge.va.x,
            y: he.edge.vb.y - he.edge.va.y
          },
          cell.site,
          lhOrthogLineDirection
        ) || vector2dIntersection(
          he.edge.vb,
          {
            x: he.edge.va.x - he.edge.vb.x,
            y: he.edge.va.y - he.edge.vb.y
          },
          cell.site,
          lhOrthogLineDirection
        );

      return {
        edge: he.edge,
        isVaInside,
        isVbInside,
        intersectionPoint
      };
    });

    const projectionLengths = projectionInfo.map(function(info) {
      const edge = info.edge;
      const isVaInside = info.isVaInside;
      const isVbInside = info.isVbInside;
      const intersectionPoint = info.intersectionPoint;

      let edgeLength = 0;

      if (isVaInside && isVbInside) {
        if (intersectionPoint) {
          const p1 = edge.va;
          const p2 = edge.vb;

          // 0. p1 and p2 are the points of the edge, CP is cell position
          // 1. extend the edge such that it does intersect the orthog
          // 2. find the intersection point (call it IP)
          // 3. rebase all vectors such that the intersection is the origin
          // 4. find the projection of the vector from IP to p1 (X)
          // 5. find the projection of the vector from IP to p2 (Y)
          // 6. the actual projection length is the difference Abs(X - Y)

          // project the edge onto
          const ipToP1 = { // A
            x: p1.x - intersectionPoint.x,
            y: p1.y - intersectionPoint.y
          };
          const ipToP2 = { // B
            x: p2.x - intersectionPoint.x,
            y: p2.y - intersectionPoint.y
          };
          const ipToCp = { // C
            x: cell.site.x - intersectionPoint.x,
            y: cell.site.y - intersectionPoint.y
          };

          const projAtoC = computeProjection(ipToP1, ipToCp);
          const aToCLength = distance(projAtoC, {x: 0, y: 0});

          const projBtoC = computeProjection(ipToP2, ipToCp);
          const bToCLength = distance(projBtoC, {x: 0, y: 0});

          const longestProj = aToCLength > bToCLength ? projAtoC : projBtoC;
          const shortestProj = aToCLength > bToCLength ? projBtoC : projAtoC;

          edgeLength = Math.abs(aToCLength - bToCLength);
        } else {
          edgeLength = distance(edge.va, edge.vb);
        }
      } else if (isVaInside || isVbInside) {
        const p1 = isVaInside ? edge.va : edge.vb;
        const p2 = isVaInside ? edge.vb : edge.va;

        if (intersectionPoint) {
          // 0. ip is the intersection within the edge, p1 is the edge point in bounds, CP is cell position
          // 1. find p1, which is the intersection of the orthog to the edge
          // 2. rebase all vectors such that the intersection is the origin
          // 3. find the projection of the vector from IP to p1

          // project the edge onto
          const vec1 = {
            x: p1.x - intersectionPoint.x,
            y: p1.y - intersectionPoint.y
          };
          const vectest1 = {
            x: p2.x - intersectionPoint.x,
            y: p2.y - intersectionPoint.y
          };
          const vec2 = {
            x: cell.site.x - intersectionPoint.x,
            y: cell.site.y - intersectionPoint.y
          };

          const projA = computeProjection(vec1, vec2);
          edgeLength = distance(projA, {x: 0, y: 0});
        } else {
          edgeLength = distance(p1, intersectionPoint);
        }
      } else {
        // no transfer for you!
        edgeLength = 0;
      }

      summedProjections += edgeLength;
      return edgeLength;
    });

    // {neighborId, transferRatio}
    const transferTo = cell.halfedges
      .filter(he => !!he.edge.lSite && !!he.edge.rSite)
      .map(function(he, heIndex) {
        const neighborId = he.edge.lSite._id === cell.site._id ? he.edge.rSite._id : he.edge.lSite._id;

        const edgeLength = projectionLengths[heIndex];
        const transferRatio = edgeLength / summedProjections;

        return {neighborId, transferRatio};
      });

    cell.simTransferTo = transferTo;
  });

  console.log('Defined starting conditions for weather (wind, temperature, moisture)');

  function getTotalMoisture() {
    return cells.reduce(function(sum, cell) {
      return sum + cell.moisture.contains;
    });
  }

  function simulateMoistureTransfer() {
    // for each cell, transfer stuff as possible!
    cells.forEach(function(cell) {
      // cell.moisture = {
      //   contains: cell.isWater ? 10.0 : 0.0, //
      //   retains: 2.0, // TODO: cooler, higher regions pull more.
      //   totalTransfered: 0
      // };
      const moistureToTransfer = cell.moisture.contains - cell.moisture.retains;

      // we always just dump it all (cept what we keep)
      if (moistureToTransfer > 0) {
        let transferred = 0;
        let transferredArray = [];

        const transferTo = cell.simTransferTo;
        transferTo.forEach(function(transferInfo) {
          const neighborCell = idToCell[transferInfo.neighborId];

          // floor to 4 decimal places, this way we know there is never any left over
          let transferAmount = transferInfo.transferRatio * moistureToTransfer;

          // water loses 50% just to prevent water building up
          if (neighborCell.isWater) {
            transferAmount *= waterPersistencyMultiplier;
          }

          neighborCell.moisture.contains += transferAmount;
          transferredArray.push({
            ratio: transferInfo.transferRatio,
            amount: transferAmount
          });
          transferred += transferAmount;
        });

        cell.moisture.totalTransfered += moistureToTransfer;
        cell.moisture.contains = cell.moisture.retains;
      }
    });
  }

  // run the simulation a bunch of times to really spread the moisture aroud
  let totalMoisture = getTotalMoisture();
  for (let i = 0; i < 300; i++) {
    simulateMoistureTransfer();

    const oldTotalMoisture = totalMoisture;
    totalMoisture = getTotalMoisture();
    if (Math.abs(oldTotalMoisture - totalMoisture) < 1) {
      break;
    }
  }

  console.log('Finished climate simulation');
};

function computeProjection(vec1, vec2) {
  const multiplier = (vector2dDotProd(vec1, vec2) / vector2dDotProd(vec2, vec2));
  return {
    x: (vec2.x * multiplier),
    y: (vec2.y * multiplier)
  };
}

function vector2dDotProd(v1, v2) {
  return v1.x * v2.x + v1.y * v2.y;
}

function vector2dIntersection(p1, d1, p2, d2) {
  // make sure a solution even exists
  const divisor = (d1.y * d2.x) - (d1.x * d2.y);

  // if divisor is 0, then no solution exists
  // the lines are parallel or the direct vectors have no solution
  if (divisor === 0) {
    return null;
  }

  const c = {
    x: p1.x - p2.x,
    y: p1.y - p2.y
  };

  const t = ((c.x * d2.y) - (c.y * d2.x)) / divisor;

  return {
    x: p1.x + (d1.x * t),
    y: p1.y + (d1.y * t)
  };
};


// returns from -179.999.. to +180
function normalizeAngle(angle) {
  if (angle > Math.PI) {
    return angle - (Math.PI * 2.0);
  } else if (angle <= -Math.PI) {
    return angle + (Math.PI * 2.0);
  } else {
    return angle;
  }
}

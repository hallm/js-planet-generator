'use strict';

module.exports = function determineBiomes(rng, starInfo, planetInfo, mapInfo, idToCell, cells) {
  cells.forEach(function(cell) {
    const temperature = cell.temperature;
    const precipitation = (cell.moisture.totalTransfered + cell.moisture.contains) * 10.0;

    cell.biome = cell.isWater ? 'Water' : whittakerDiagram(temperature, precipitation);
  });
};

// very rough, but close enough
function whittakerDiagram(temperature, precipitation) {
  // break the table
  if (temperature > 295) {
    if (precipitation < 40) {
      return 'Desert';
    } else if (precipitation < 120) {
      return 'Savana';
    } else if (precipitation < 250) {
      return 'Tropical Forest';
    } else {
      return 'Tropical Rain Forest';
    }
  } else if (temperature > 285) {
    if (precipitation < 20) {
      return 'Desert';
    } else if (precipitation < 100) {
      return 'Savana';
    } else if (precipitation < 225) {
      return 'Temperate Deciduous Forest';
    } else {
      return 'Temperate Rain Forest';
    }
  } else if (temperature > 280) {
    if (precipitation < 10) {
      return 'Desert';
    } else if (precipitation < 60) {
      return 'Savana';
    } else if (precipitation < 200) {
      return 'Temperate Deciduous Forest';
    } else {
      return 'Temperate Rain Forest';
    }
  } else if (temperature > 275) {
    if (precipitation < 5) {
      return 'Desert';
    } else if (precipitation < 40) {
      return 'Savana';
    } else {
      return 'Tiaga';
    }
  } else if (temperature > 270) {
    if (precipitation < 25) {
      return 'Savana';
    } else {
      return 'Tiaga';
    }
  } else if (temperature > 260) {
    return 'Tundra';
  } else {
    return 'Ice';
  }
}

'use strict';

const randomInt = require('../utils/random-int');
const distance = require('../utils/distance');
const RandomQueue = require('../utils/random-queue');

module.exports = function plateTectonics(rng, starInfo, planetInfo, mapInfo, idToCell, cells) {
  // determine some random num of plates, maybe a 2d6 ?
  // find the starting cell for all plates, add them to a RandomQueue
  // while there are items in the queue:
  //   pop one cell
  //   add a neighbor (the half edges .site will be the neighbor)
  //   if the neighbor has any unassigned neighbors, add it to the queue
  //   if the cell has any unassigned neighbors, add it to the queue

  const percentSurfaceWater = planetInfo.percentSurfaceWater || 0;
  let continentMultiplier = 1;
  if (planetInfo.planetDiameter < 9000) {
    continentMultiplier /= 2;
  } else if (planetInfo.planetDiameter > 15000) {
    continentMultiplier *= 1.5;
  }

  if (percentSurfaceWater < 30) {
    continentMultiplier /= 2;
  } else if (percentSurfaceWater > 60) {
    continentMultiplier *= 1.5;
  }

  // need to use ceil, because rolling a 1 then * 1/4 is possible.
  const numberOfContinents = Math.ceil(randomInt(rng, 1, 6) * continentMultiplier);

  // we double it to get more interesting landscapes
  const numberOfContinentPlates = percentSurfaceWater === 100 ? 0 : numberOfContinents * 2;

  // compute the number of total plates such that the number of land continents is the % of land
  let numberOfPlates;

  if (percentSurfaceWater === 100) {
    numberOfPlates = numberOfContinents * 2;
  } else if (percentSurfaceWater === 0) {
    numberOfPlates = numberOfContinentPlates;
  } else {
    numberOfPlates = Math.ceil(numberOfContinentPlates / (100 - percentSurfaceWater) * 100);
  }

  console.log(numberOfContinentPlates, numberOfPlates);

  const plateInfo = new Array(numberOfPlates);
  const plateRandomQueue = new RandomQueue(rng);

  // figure out roughly how much land area there is, so we can stop adding to all land continents
  const totalMapArea = planetInfo.planetSurfaceArea;
  const roughLandArea = totalMapArea * (100 - planetInfo.percentSurfaceWater) / 100;
  let currentLandArea = 0;

  // randomly generate some plates
  for (let i = 0; i < numberOfPlates; i++) {
    // randomly generate a land-height & water-depth of each plate
    // a direction the plate moves (angle)
    // and a speed in cm that the plate moves per year
    const randomLandHeight = rng() * 4.0;
    const randomWaterDepth = rng() * 3.0;
    const randomAngle = rng() * 360.0;
    const speedInCm = rng() * 10.0;
    const isWater = i >= numberOfContinentPlates;

    console.log('plate ', i, randomAngle, isWater);

    let randomCellIndex = null;
    let cell = null;

    // do-while in case we some how, by random chance, pick the same cell twice
    do {
      randomCellIndex = randomInt(rng, 0, cells.length - 1);
      cell = cells[randomCellIndex];
    } while (!cell  || cell.plateIndex != null);

    cell.plateIndex = i;
    cell.isWater = isWater;
    cell._startingPlate = true;

    plateInfo[i] = {
      plateIndex: i,
      area: 0, // will be computed later, after edge cleaning
      isWater: isWater,
      landHeight: randomLandHeight,
      waterDepth: randomWaterDepth,
      direction: randomAngle,
      speed: speedInCm,
      cells: [cell]
    };

    if (!isWater) {
      currentLandArea += cell.area;
    }
    plateRandomQueue.push(cell);
  }

  // possibly, first assign all the land masses
  // THEN assign the water plates?
  // or alternatively, generate all the plates, then later figure out which ones are water
  // it is conceivable to have 180 plates (18 continents with 90% surface water)
  // so might be, do the land masses first
  // problem with generating land masses THEN water is the land masses could end up eating too much
  // and creating lonely cells

  let northPoleClaimedBy = null;
  let southPoleClaimedBy = null;
  while (!plateRandomQueue.isEmpty()) {
    const randcell = plateRandomQueue.pop();
    const cell = idToCell[randcell.site._id];

    let cellNeighbors = cell._neighbors;
    // only used immediate neighbors if our plate did not claim the north pole if it is already claimed
    if ((cell._isOnNorth && northPoleClaimedBy != null && cell.plateIndex !== northPoleClaimedBy) || (cell._isOnSouth && southPoleClaimedBy != null && cell.plateIndex !== southPoleClaimedBy)) {
      cellNeighbors = cell._attachedNeighbors;
    }

    const possibles = cellNeighbors
      .map(cellid => idToCell[cellid])
      .filter(site => site.plateIndex == null);

    if (possibles.length === 0) {
      continue;
    }

    const randomNeighborIndex = randomInt(rng, 0, possibles.length - 1);
    const neighborCell = possibles[randomNeighborIndex];

    // if this connection crosses over a pole, then NO OTHER plate can cross over that pole
    const doesCrossNorthPole = cell._isOnNorth && cell._attachedNeighbors.indexOf(neighborCell.site._id) === -1;
    const doesCrossSouthPole = cell._isOnSouth && cell._attachedNeighbors.indexOf(neighborCell.site._id) === -1;

    const plate = plateInfo[cell.plateIndex];
    const isWater = plate.isWater;

    if (doesCrossNorthPole) {
      northPoleClaimedBy = cell.plateIndex;
    }
    if (doesCrossSouthPole) {
      southPoleClaimedBy = cell.plateIndex;
    }

    neighborCell.plateIndex = cell.plateIndex;
    // assign water cell later
    // neighborCell.isWater = isWater;

    plate.cells.push(neighborCell);

    plateRandomQueue.push(cell);
    plateRandomQueue.push(neighborCell);

    // check if we have enough land. if so, we will continue on to generating water tiles
    if (!isWater) {
      currentLandArea += neighborCell.area;

      if (currentLandArea > roughLandArea) {
        plateRandomQueue.values = plateRandomQueue.values.filter(p => p.isWater);
      }
    }
  }

  // there is a possibility of unassigned cells that are stuck between land cells
  // just going to assign them to the nearest neighbor
  let unassignedCells = cells.filter(cell => cell.plateIndex == null);

  while (unassignedCells.length) {
    for (let i = 0; i < unassignedCells.length; i++) {
      const cell = unassignedCells[i];

      // assign it to any random neighbor
      const cellNeighbors = cell._attachedNeighbors || cell._neighbors;
      const possibles = cellNeighbors
        .map(cellid => idToCell[cellid])
        .filter(site => site.plateIndex != null);

      if (!possibles || !possibles.length) {
        continue;
      }

      const randomNeighborIndex = possibles.length === 1 ? 0 : randomInt(rng, 0, possibles.length - 1);
      const neighborCell = possibles[randomNeighborIndex];
      const plate = plateInfo[neighborCell.plateIndex];

      cell.plateIndex = neighborCell.plateIndex;
      plate.cells.push(cell);
    }

    unassignedCells = cells.filter(cell => cell.plateIndex == null);
  }

  console.log('North pole claimed by:', northPoleClaimedBy);
  console.log('South pole claimed by:', southPoleClaimedBy);

  // next need to clean up the edges
  function cleanPlateEdges() {
    cells.forEach(function(cell) {
      const _id = cell._id;
      const currentPlate = cell.plateIndex;
      const neighborPlates = {};
      let totalEdgeLength = 0;

      cell.halfedges
        .forEach(function(he) {
          const neighborSite = he.edge.lSite._id === _id ? he.edge.rSite : he.edge.lSite;
          let neighborPlateIndex = -1;

          if (neighborSite != null) {
            neighborPlateIndex = idToCell[neighborSite._id].plateIndex;
          } else if (cell._isOnNorth && he.edge.vb.y <= 0.0 && he.edge.va.y <= 0.0) {
            neighborPlateIndex = northPoleClaimedBy;
          } else if (cell._isOnSouth && he.edge.vb.y >= mapInfo.mapHeight && he.edge.va.y >= mapInfo.mapHeight) {
            neighborPlateIndex = southPoleClaimedBy;
          } else {
            // in other cases? just don't add the edge
            return;
          }

          const edgeLength = distance(he.edge.vb, he.edge.va);

          totalEdgeLength += edgeLength;

          if (!neighborPlates[neighborPlateIndex]) {
            neighborPlates[neighborPlateIndex] = edgeLength;
          } else {
            neighborPlates[neighborPlateIndex] += edgeLength;
          }
        });

        const plateKeys = Object.keys(neighborPlates).map(k => parseInt(k));
        plateKeys.sort((a, b) => neighborPlates[b] - neighborPlates[a]);
        const bestPlate = plateKeys[0];

        // only change if the difference is clearly larger (like double)
        const diffInEdge = (neighborPlates[bestPlate] - neighborPlates[currentPlate]) / neighborPlates[currentPlate];

        if (bestPlate !== currentPlate && (diffInEdge > 1.0)) {
          const plate = plateInfo[currentPlate];
          plate.cells = plate.cells.filter(c => c.site._id !== _id);

          plateInfo[bestPlate].cells.push(cell);
          cell.plateIndex = bestPlate;
        }
    });
  }
  // disabling this for now until Plates 2.0 is done, then will revisit cleaning edges
  // cleanPlateEdges();
  // cleanPlateEdges();

  console.log('Cleaned up plate edges');

  currentLandArea = plateInfo.reduce(function(sum, plate) {
    plate.area = plate.cells.reduce(function(area, cell) {
      cell.isWater = plate.isWater;

      return area + cell.area;
    }, 0);

    if (plate.isWater) {
      return sum;
    }

    return sum + plate.area;
  }, 0);

  console.log('estimated (rolled) water percent:', planetInfo.percentSurfaceWater);
  console.log('total land mass percent to water:', (currentLandArea / totalMapArea * 100));

  // need to compute the "force" vector for each cell:
  // 1. redefine cells in terms of angle and distance on the spheroid to the plate's "root" cell
  // 2. compute the new location of the "root" cell based on the plate's force vector
  // 3. compute the locations of all other cells using the new "root" location with the angle+distance
  // 4. determine the vector for the other cell's based on their new location

  const mercator = mapInfo.mercator;

  plateInfo.forEach(function(plateInfo) {
    const forceDirection = plateInfo.direction * Math.PI / 180.0;

    // speed is in cm/yr and yes, we would divide by 100000 to get KM.
    // but lets say these forces have acted over 2,000,000 years
    // so multiply by 20 (2m / 100k = 20).
    const forceDist = plateInfo.speed * 20;

    const rootCell = plateInfo.cells[0];
    const rootCellCoord = rootCell.site.coord;
    const rootCellFinalCoord = mercator.pointFrom(rootCellCoord, forceDirection, forceDist);
    const rootCellFinalPos = mercator.unproject(rootCellFinalCoord);
    rootCell._force = {
      x: rootCellFinalPos.x - rootCell.site.x,
      y: rootCellFinalPos.y - rootCell.site.y
    };

    for (let i = 1; i < plateInfo.cells.length; i++) {
      const cell = plateInfo.cells[i];
      const cellCoord = cell.site.coord;
      // first point is always the root cell
      const angle = mercator.angleBetweenPoints(rootCellCoord, cellCoord);
      const distance = mercator.coordDistance(rootCellCoord, cellCoord);

      const newPointCoord = mercator.pointFrom(rootCellFinalCoord, angle, distance);
      const cellFinalPos = mercator.unproject(newPointCoord);
      cell._force = {
        x: cellFinalPos.x - cell.site.x,
        y: cellFinalPos.y - cell.site.y
      };
    }
  });

  // use the plates to find plate-interactions and build valleys and mountains
  // lerp the differences in height (coast to height; mountain/valley to height;)
  // for each cell, compute the pressure on that cell?
  let maxP = -99999;
  let minP = 99999;

  cells.forEach(function(cell) {
    let isConnectedToLand = false;
    let isConnectedToWater = false;

    let netPressure = 0;
    cell.halfedges.forEach(function(halfedge) {
      if (halfedge._pressure != null) {
        netPressure += halfedge._pressure;
        return;
      }

      // generate the pressure on this edge
      // p1 = cell's point
      // f1 = cell's force vector
      // p2 = neighbor's point
      // f2 = neighbor's force vector
      // pressure = dist(p1, p2) - dist(f1, f2)

      const lSite = halfedge.edge.lSite;
      const rSite = halfedge.edge.rSite;

      if (!lSite || !rSite) {
        // TODO: what about poles?
      } else {
        const cell1 = idToCell[lSite._id];
        const cell2 = idToCell[rSite._id];

        // force is only between cells of different plates
        if (cell1.plateIndex === cell2.plateIndex) {
          return;
        }

        if (cell.site._id === lSite._id) {
          isConnectedToLand = isConnectedToLand || !cell2.isWater;
          isConnectedToWater = isConnectedToWater || cell2.isWater;
        } else {
          isConnectedToLand = isConnectedToLand || !cell1.isWater;
          isConnectedToWater = isConnectedToWater || cell1.isWater;
        }

        const p1 = cell1.site;
        const p2 = cell2.site;
        const f1 = {
          x: cell1._force.x + cell1.site.x,
          y: cell1._force.y + cell1.site.y
        };
        const f2 = {
          x: cell2._force.x + cell2.site.x,
          y: cell2._force.y + cell2.site.y
        };

        // TODO: better handle sliding, subduction vs collisions.
        // also, is it possible for subduction to push the "pressure" more inland?
        halfedge._pressure = distance(p1, p2) - distance(f1, f2);
        netPressure += halfedge._pressure;
      }
    });

    maxP = Math.max(maxP, netPressure);
    minP = Math.min(minP, netPressure);

    if (netPressure > 4) {
      // subduction is going to create land on the border to push mountains inland
      if (cell.isWater && isConnectedToLand) {
        cell.isWater = false;
      } else {
        // TODO: determining the actual height?
        // I called it "pressure" but we are working with cm/yr over 2mil years
        // should be able to use a formula to come up a real height for this cell
        // also, should the height be computed for each vertex?
        // or compute for the center, and LERP/SLERP later?
        cell._mountainHeight = 1;
      }
    } else if (netPressure < -4) {
      // TODO: what about water "reclaiming" a valley and/or inland lakes?
      cell._valleyDepth = 1;
    }
  });

  currentLandArea = cells.reduce(function(sum, cell) {
    return sum + (cell.isWater ? 0 : cell.area);
  }, 0);

  // note: I somewhat feel like the percent water could be held like a "guideline"
  // this way we could get more variations.
  // or could spawn lakes / add land if desired
  console.log('final land mass percent to water:', (currentLandArea / totalMapArea * 100));
};

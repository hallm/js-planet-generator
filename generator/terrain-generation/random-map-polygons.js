'use strict';

const Mercator = require('../utils/mercator');
const Poisson = require('../utils/poisson/');
const Voronoi = require('../utils/voronoi/');

// generate some random polygons to make computing regions easy
// Using a Poisson Disc Sampler as it generates the "cellular" effect
// similar to Perlin + Lloyd Relaxation.
module.exports = function randomMapPolygons(rng, starInfo, planetInfo, mapInfo) {
  const mapWidth = mapInfo.mapWidth;
  const mapHeight = mapInfo.mapHeight;

  const poissonPointGeneratorOptions = {
    minDistance: 15, // this is in km
    generatePerPoint: 40
  };

  const planetRadius = planetInfo.planetDiameter / 2.0;
  planetInfo.planetSurfaceArea = (4 * Math.PI * planetRadius * planetRadius);

  const mercator = new Mercator(planetRadius, mapWidth, mapHeight);
  mapInfo.mercator = mercator;
  const generator = new Poisson(mapWidth, mapHeight, rng, mercator.distance.bind(mercator));

  const points = generator.generatePoisson(
    poissonPointGeneratorOptions.minDistance,
    poissonPointGeneratorOptions.generatePerPoint
  );

  points.forEach(function(point, index) {
    point._id = index;
    point.coord = mercator.project(point);
  });

  console.log('generated:', points.length);

  // add extra points on the left/right for the wrap around effect
  const pointsWithWrappedSide = points.concat(
    points.reduce(function(extras, point) {
      if (point.x <= (mapWidth / 4.0)) {
        extras.push({
          x: point.x + mapWidth,
          y: point.y,
          coord: point.coord,
          _id: point._id
        });
      } else if (point.x >= (mapWidth * 3.0 / 4.0)) {
        extras.push({
          x: point.x - mapWidth,
          y: point.y,
          coord: point.coord,
          _id: point._id
        });
      }

      return extras;
    }, [])
  );

  const voronoi = new Voronoi();
  const diagram = voronoi.compute(pointsWithWrappedSide, {
    xl: -(mapWidth / 4.0),
    xr: (mapWidth * 5.0 / 4.0),
    yt: 0,
    yb: mapHeight
  });
  console.log('computed voronoi');

  // for each cell, generate it's polygon and bounding box
  diagram.cells.forEach(function(cell) {
    const startPoint = cell.halfedges[0].getStartpoint();

    let minx = startPoint.x;
    let miny = startPoint.y;
    let maxx = startPoint.x;
    let maxy = startPoint.y;

    // with WGS84:
    // (lng[1] - lng[0]) * (2 + sin(lat[0]) + sin(lat[1])) + ... + (lng[n+1 || 0] - lng[n]) * (2 + sin(lat[n]) + sin(lat[n+1 || 0]))
    // then finally, abs(area * planetRadiusKm * planetRadiusKm / 2.0)
    let area = 0; // in sqkm

    const polygon = cell.halfedges.reduce((p, he) => {
      const startPoint = he.getStartpoint();
      const endPoint = he.getEndpoint();

      // set the lat,lng of each point while we are at it
      startPoint.coord = mercator.project(startPoint);
      endPoint.coord = mercator.project(endPoint);

      // because of how the polygon is stored, it wraps around to p[0] for us
      // basically, the startpoint,endpoint of halfedge[0] is p[0],p[1]
      // and the startpoint,endpoint of halfedge[n] is p[n],p[0]
      const dLngRad = (endPoint.coord.lng - startPoint.coord.lng) * Math.PI / 180.0;
      area += dLngRad * (2.0 + Math.sin(startPoint.coord.lat * Math.PI / 180.0) + Math.sin(endPoint.coord.lat * Math.PI / 180.0));

      minx = Math.min(minx, Math.floor(endPoint.x));
      miny = Math.min(miny, Math.floor(endPoint.y));
      maxx = Math.max(maxx, Math.ceil(endPoint.x));
      maxy = Math.max(maxy, Math.ceil(endPoint.y));

      if (endPoint !== startPoint) {
        p.push(endPoint);
      }

      return p;
    }, [startPoint]);

    const polyBounds = {
      minx: minx,
      maxx: maxx,
      miny: miny,
      maxy: maxy
    };

    cell.points = polygon;
    cell.bounds = polyBounds;
    cell.area = Math.abs(area * planetRadius * planetRadius / 2.0);
  });

  return diagram;
};

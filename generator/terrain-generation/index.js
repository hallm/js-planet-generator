'use strict';

const climateGeneration = require('./climate');
const determineBiomes = require('./determine-biomes');
const determineNeighbors = require('./determine-neighbors');
const plateTectonics = require('./plate-tectonics');
const randomMapPolygons = require('./random-map-polygons');
const rngFactory = require('../utils/rng-factory');

const mapWidth = 900;
const mapHeight = mapWidth;

module.exports = function terrainGeneration(seed, starInfo, planetInfo) {
  if (!seed) {
    seed = (Math.random() * 1000000000) | 0;
  }

  const rng = rngFactory(seed);

  const mapInfo = {
    mapWidth: mapWidth,
    mapHeight: mapHeight
  };

  const diagram = randomMapPolygons(rng, starInfo, planetInfo, mapInfo);
  mapInfo.cells = diagram.cells;

  const idToCell = {};
  const cells = [];
  diagram.cells.forEach(function(cell) {
    const point = cell.site;

    if ((point.x >= 0 && point.x < mapWidth && point.y >= 0 && point.y < mapHeight)) {
      idToCell[cell.site._id] = cell;
      cells.push(cell);
    }
  });

  mapInfo.idToCell = idToCell;

  determineNeighbors(rng, starInfo, planetInfo, mapInfo, idToCell, cells);
  plateTectonics(rng, starInfo, planetInfo, mapInfo, idToCell, cells);
  climateGeneration(rng, starInfo, planetInfo, mapInfo, idToCell, cells);
  determineBiomes(rng, starInfo, planetInfo, mapInfo, idToCell, cells);

  return mapInfo;
};
